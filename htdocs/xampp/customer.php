<?php
require("PasswordHash.php");

$user = $_POST['name'];
$pass = $_POST['pwd']; 
// Base-2 logarithm of the iteration count used for password stretching
$hash_cost_log2 = 8;
// Do we require the hashes to be portable to older systems (less secure)?
$hash_portable = FALSE;
$hasher = new PasswordHash($hash_cost_log2, $hash_portable);
$hash = $hasher->HashPassword($pass);



try{
    // Create a new connection.
    // You'll probably want to replace hostname with localhost in the first parameter.
    // The PDO options we pass enable do the following:
    // \PDO::ATTR_ERRMODE enables exceptions for errors.  This is optional but can be handy.
    // \PDO::ATTR_PERSISTENT disables persistent connections, which can cause concurrency issues in certain cases.  See "Gotchas".
    // \PDO::MYSQL_ATTR_INIT_COMMAND alerts the connection that we'll be passing UTF-8 data.  This may not be required depending on your configuration, but it'll save you headaches down the road if you're trying to store Unicode strings in your database.  See "Gotchas".
    $link = new \PDO('mysql:host=localhost;dbname=convenientwebsite', 
                     'root', 
                     'buf111', 
                     array(
                            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION, 
                            \PDO::ATTR_PERSISTENT => false, 
                            \PDO::MYSQL_ATTR_INIT_COMMAND => 'set names utf8mb4'
                    )
                    );
 
    $handle = $link->prepare('insert into CustomerSample (Name, Password) values (?, ?)');
 
    // PHP bug: if you don't specify PDO::PARAM_INT, PDO may enclose the argument in quotes.  This can mess up some MySQL queries that don't expect integers to be quoted.
    // See: https://bugs.php.net/bug.php?id=44639
    // If you're not sure whether the value you're passing is an integer, use the is_int() function.
    $handle->bindParam(1, $user, PDO::PARAM_STR, 12);
    $handle->bindParam(2, $hash, PDO::PARAM_STR);
    $handle->execute();
 
    // Using the fetchAll() method might be too resource-heavy if you're selecting a truly massive amount of rows.
    // If that's the case, you can use the fetch() method and loop through each result row one by one.
    // You can also return arrays and other things instead of objects.  See the PDO documentation for details.
}
catch(\PDOException $ex){
    print($ex->getMessage());
}

echo "Welcome";

?>
