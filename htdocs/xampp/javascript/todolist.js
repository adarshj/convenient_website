$document.ready(function() {
	//Delete button click event
	
	$('#addNew').click(function() {
		addNewRow();
	});

	$('.deleteButton').click(function() {
		deleteRow($(this));
	});
});


function addNewRow() {
	
	var numRows = $('#task_list tr').length;
	$('#task_list').append('
<tr>
<td><input type="text" id="title-'+numRows+'" /></td>
</tr>
');
}


function deleteRow(thisButton){
	thisButton.parent().parent().remove();
}


